// Initialize Firebase
var config = {
apiKey: "AIzaSyCzlEpgvT3IUvTxTs7pJUD81CFYvAXwOzU",
authDomain: "cde-assignment-part2.firebaseapp.com",
databaseURL: "https://cde-assignment-part2.firebaseio.com",
projectId: "cde-assignment-part2",
storageBucket: "cde-assignment-part2.appspot.com",
messagingSenderId: "657682977144"
};
firebase.initializeApp(config);
window.onload = function(){
    var data;//store the json from firebase
    var link = firebase.database().ref().child('user');
    link.once('value').then(function(snapshot){//request the json form firebase
        data = snapshot.val();
    });
    
    function setCookie(cname,cvalue,exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    
    
    $(document).on('click','#btnlogin',function() {
        var email = document.getElementById("lemail").value;
        var pass = document.getElementById("lpassword").value;
        var chkEmail = false;
        var chkPass = false;
        if(email != "" && pass != ""){
            for(var key in data){
                if(data[key].email == email){
                    chkEmail = true;
                    if(data[key].password == pass){
                        chkPass = true;
                    }
                }
            }
        }
        
        var chkEmailStr = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        if(email == "" || pass == ""){
            alert("Please input all text field");
        }else if(!chkEmailStr.test(String(email).toLowerCase())){
            alert("Plase input a valid email!");
        }else if(!chkEmail){
            alert("The email does not already registered");
        }else if(!chkPass){
            alert("Wrong password");
        }else if(chkEmail && chkPass){
            setCookie("email",email,1);
            window.location.replace('/');
        }

    });
    
    
    $(document).on('click','#btnregi',function() {
        function chkDupEmail(email){
            var isDuplicate = false;
            for(var key in data){
                //alert("firebase:"+data[key].email+" email:"+email);
                if(data[key].email == email)
                    isDuplicate = true;
            }
            return isDuplicate;
        }
        
        var email = document.getElementById("remail").value;
        var pass = document.getElementById("rpassword").value;
        var rpass = document.getElementById("rrpassword").value;
        //the regex for checking the email format
        var chkEmailStr = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        if(email == "" || pass == "" || rpass == ""){
            alert("Please input all text field");
        }else if(pass != rpass){
            alert("Two passwords are not the same ")
        }else if(!chkEmailStr.test(String(email).toLowerCase())){
            alert("Plase input a valid email!");
        }else if(chkDupEmail(email)){
            alert("This email is already registered!");
        }else{
            //create an Object "person" to store user's input
            var person = {};
            person.email = email;
            person.password = pass;
            //person.vcode = Math.floor(Math.random() * 1000000);
            //person.active = false;
            //set the path "user" under root
            var ref = firebase.database().ref('/user');
            //push the Object into firebase
            ref.push(person);
            alert("The account created success");
            window.location.reload();
        }
    });    

}