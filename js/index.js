

var app = angular.module('weather', ['ngRoute','ngCookies']);

app.config(function($routeProvider){
    $routeProvider.when('/',{
        templateUrl:'weatherTable.html',
        controller:'getWeather'
    }).when('/bookmarkAdd',{
        templateUrl:'bookmarkAdd.html',
        controller:'bookmark'
    }).when('/bookmarkUpdate',{
        templateUrl:'bookmarkUpdate.html',
        controller:'bookmark'
    }).when('/bookmarkDelete',{
        templateUrl:'bookmarkDelete.html',
        controller:'bookmark'
    }).when('/login',{
        templateUrl:'login.html'
    }).when('/registration',{
        templateUrl:'registration.html'
    }).otherwise({
        redirectTo:'/'
    });
});

//check user login
app.controller('chkCookie',function($scope, $cookies) {
    //get cookie
    var email = $cookies.get('email');
    //show the login link
    if(email == "" || email == undefined){
        $scope.email = "Login";
    }else{//show the user's email and link to bookmark list
        var el =angular.element(document.querySelector('.login'));
        el.attr('href', '#!/bookmarkAdd');
        $scope.email = $cookies.get('email');
    }
    
});

//logout, delete cookie and redirect to index
app.controller('logout',function($scope, $cookies) {
    $scope.logout = function(){
        $cookies.remove('email');
        window.location.replace('/');
    }
});

//get and search city weather information
app.controller('getWeather', function($scope,$http) {
    //get today
    var now = new Date();
    var today = now.getDate()+"/"+now.getMonth()+"/"+now.getFullYear();

    //get the default weather information
    $http({
        method : "GET",
        url : "https://desolate-dawn-76073.herokuapp.com/getWeather"
    }).then(function(data, status, headers, config) {
        console.log((data.data.result));
        $scope.weatherinfos = data.data.result;
        $scope.today = today;
    },
    function (status) {
        $scope.error = status;
        console.log($scope.error);
    });
    
    //search function for search the city weather information
    $scope.search = function(){
        //get city name from textfield
        var city = document.getElementById('tfCityname').value;
        //city name is empty
        if(city == "" || city == undefined){
            alert("Please input the city name!");
        }else{//get the city weather information
            $http({
                method : "GET",
                url : "https://desolate-dawn-76073.herokuapp.com/getWeather?city="+city
            }).then(function(data, status, headers, config) {
                console.log(data.data.success);
                if(data.data.success == false){
                    alert(data.data.message);
                    window.location.reload();
                }
                $scope.weatherinfos = data.data.result;
                $scope.today = today;
            },
            function (status) {
                $scope.error = status;
                console.log($scope.error);
            });
        }
    };
});

//bookmark management: add, update, delete
app.controller('bookmark', function($scope,$http,$cookies) {
    var email = $cookies.get('email');
    
    //add city
    $scope.add = function(){
        //get the city name
        var city = document.getElementById("tfCityname").value;
        //check the city name is empty
        if(city == "" || city == undefined){
            alert("Please Input The City Name!");
        }else{//send the add request to api
            $http({
                method : "POST",
                url : "https://desolate-dawn-76073.herokuapp.com/addToBookmark",
                //url : "https://assignment-part2-lamchunyau.c9users.io/addToBookmark",
                data : {"city":city,"email":email},
                headers: {'Content-Type': 'application/json','Accept': 'application/json'}
            }).then(function(data, status, headers, config) {
                console.log((data));
                if(data.data.success == false){
                    alert(data.data.message);
                }
                window.location.reload();
            },
            function (status) {
                console.log(status);
            });
        }
    };
    
    //update city
    $scope.update = function(){
        //get old city name and new city name
        var oldcity = document.getElementById("oldCity").value;
        var newcity = document.getElementById("newCity").value;
        //check old city name is empty
        if(oldcity == "" || oldcity == undefined){
            alert("Plese Input Old City Name!");
        //chekc new city name is empty
        }else if(newcity == "" || newcity == undefined){
            alert("Plese Input New City Name!");
        }else{//send the update request to api
            $http({
                method : "PUT",
                url : "https://desolate-dawn-76073.herokuapp.com/updateToBookmark",
                //url : "https://assignment-part2-lamchunyau.c9users.io/updateToBookmark",
                data : {"city_old":oldcity,"city_new":newcity,"email":email},
                headers: {'Content-Type': 'application/json','Accept': 'application/json'}
            }).then(function(data, status, headers, config) {
                console.log((data));
                if(data.data.success == false){
                    alert(data.data.message);
                }
                window.location.reload();
            },
            function (status) {
                console.log(status);
            });
            
        }
    };
    
    //delete city
    $scope.delete = function(){
        //get city name
        var city = document.getElementById("tfCityname").value;
        //check the city name is empty
        if(city == "" || city == undefined){
            alert("Please Input The City Name!");
        }else{//send the delete request to api
            $http({
                method : "DELETE",
                url : "https://desolate-dawn-76073.herokuapp.com/delFromBookmark?city="+city+"&email="+email,
                //url : "https://assignment-part2-lamchunyau.c9users.io/delFromBookmark?city="+city+"&email="+email,
                headers: {'Content-Type':'application/x-www-form-urlencoded'}
            }).then(function(data, status, headers, config) {
                console.log(data);
                if(data.data.success == false){
                    alert(data.data.message);
                }
                window.location.reload();
            },
            function (status) {
                console.log(status);
            });
        }
    };
});

//get the city weather information from bookmark list
app.controller('getBookmark', function($scope,$http,$cookies) {
    //get the user's email from cookie
    var email = $cookies.get('email');
    //get today
    var now = new Date();
    var today = now.getDate()+"/"+now.getMonth()+"/"+now.getFullYear();
    console.log(email);
    //send get bookmark list request to api
    $http({
        method : "GET",
        url : "https://desolate-dawn-76073.herokuapp.com/getBookmarkList?email="+email
        //url : "https://assignment-part2-lamchunyau.c9users.io/getBookmarkList?email="+email
    }).then(function(data, status, headers, config) {
        //onsole.log((data.data.result));
        $scope.weatherinfos = data.data.result;
        $scope.today = today;
    },
    function (status) {
        $scope.error = status;
        console.log($scope.error);
    });
    
});